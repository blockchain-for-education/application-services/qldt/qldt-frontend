import { makeStyles } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { forwardRef } from "react";
import { Helmet } from "react-helmet";

const useStyles = makeStyles((theme) => ({
  root: {
    height: (props) => (props.h100 ? "100%" : "auto"),
    padding: theme.spacing(2.5),
  },
}));

const Page = forwardRef(({ children, title = "", h100, ...rest }, ref) => {
  const cls = useStyles({ h100 });

  return (
    <div ref={ref} {...rest} className={cls.root}>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      {children}
    </div>
  );
});

Page.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
};

export default Page;
