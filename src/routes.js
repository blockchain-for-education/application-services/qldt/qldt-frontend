import React from "react";
import { Navigate } from "react-router-dom";
import MainLayout from "src/layouts/MainLayout";
import StaffDashboardLayout from "src/layouts/StaffDashboardLayout";
import TeacherDashboardLayout from "src/layouts/TeacherDashboardLayout";
import NotFoundView from "src/shared/NotFoundView";
import SignInView from "src/views/guest/SignIn";
import SignUpView from "src/views/guest/SignUp";
import { getRole, ROLE } from "./utils/mng-role";
import ClassAndStudentInfo from "./views/staff/ClassTable";
import MakeRequest from "./views/staff/MakeRequest";
import StudentTable from "./views/staff/StudentTable";
import TeacherTable from "./views/staff/TeacherTable";
import UploadAttendance from "./views/staff/UploadAttendance";
import UploadSchedule from "./views/staff/UploadSchedule";
import TeacherProfile from "./views/teacher/Profile";
import SubmitSubjectPoint from "./views/teacher/SubmitSubjectPoint";

const routes = [
  {
    path: "/cb-pdt",
    element: <StaffDashboardLayout />,
    children: [
      { path: "dang-ki-tham-gia", element: <MakeRequest /> },
      { path: "upload-phan-cong-giang-day", element: <UploadSchedule /> },
      { path: "upload-ds-diem-danh", element: <UploadAttendance /> },
      { path: "ds-giang-vien", element: <TeacherTable /> },
      { path: "ds-sinh-vien", element: <StudentTable /> },
      { path: "thong-tin-lop-hoc", element: <ClassAndStudentInfo /> },

      { path: "*", element: <Navigate to="/404" replace={true} /> },
    ],
  },
  {
    path: "/giang-vien",
    element: <TeacherDashboardLayout />,
    children: [
      { path: "thong-tin-ca-nhan", element: <TeacherProfile /> },
      { path: "nhap-diem-lop-hoc", element: <SubmitSubjectPoint /> },
      { path: "*", element: <Navigate to="/404" replace={true} /> },
    ],
  },
  {
    path: "/",
    element: <MainLayout />,
    children: [
      { path: "dang-ki", element: <SignUpView /> },
      { path: "dang-nhap", element: <SignInView /> },
      { path: "404", element: <NotFoundView /> },
      { path: "/", element: <Redirector /> },
      { path: "*", element: <Navigate to="/404" replace={true} /> },
    ],
  },
];

function Redirector(props) {
  const role = getRole();
  let to = "";
  if (!role) {
    to = "/dang-nhap";
  } else if (role === ROLE.STAFF) {
    to = "/cb-pdt/dang-ki-tham-gia";
  } else if (role === ROLE.TEACHER) {
    to = "/giang-vien/thong-tin-ca-nhan";
  } else if (role === ROLE.BUREAU) {
    // TODO: complete path if you like
    to = "/giao-vu";
  }
  return <Navigate to={to} />;
}

export default routes;
