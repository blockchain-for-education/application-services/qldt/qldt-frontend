import { makeStyles } from "@material-ui/core";
import { DataGrid } from "@material-ui/data-grid";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";
import Page from "../../../shared/Page";
import axios from "axios";

const columns = [
  { field: "id", headerName: "#", width: 80, type: "string" },
  { field: "studentId", headerName: "Mã Sinh Viên", width: 120, type: "string" },
  { field: "name", headerName: "Họ và  Tên", width: 200, type: "string" },
  { field: "class", headerName: "Lớp", width: 200, type: "string" },
  { field: "academic", headerName: "Khoa/Viện", width: 200, type: "string" },
  { field: "account", headerName: "Tài khoản", width: 250, type: "string" },
  { field: "firstTimePassword", headerName: "Mật khẩu", width: 200 },
  // { field: "publicKey", headerName: "Khóa công khai", width: 300 },
];

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "white",
  },
}));

export default function StudentTable(props) {
  const cls = useStyles();
  const [state, setState] = useState({
    fetching: true,
    students: [],
  });
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    fetchStudents();
  }, []);

  async function fetchStudents() {
    try {
      const response = await axios.get("/staff/students");
      const students = response.data.map((student, index) => ({ ...student, id: index + 1 }));
      setState({ fetching: false, students: response.data.length > 0 ? students : [] });
    } catch (error) {
      enqueueSnackbar("Fail to load student " + error.response.data, { variant: "error", anchorOrigin: { vertical: "top", horizontal: "center" } });
    }
  }
  return (
    <Page title="Danh sách sinh viên">
      <DataGrid className={cls.root} loading={state.fetching} rows={state.students} columns={columns} rowHeight={48} pageSize={50} autoHeight rowsPerPageOptions={[5, 10, 25, 50, 100]}></DataGrid>
    </Page>
  );
}
