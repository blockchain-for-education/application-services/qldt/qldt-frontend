import DownloadExampleData from "../../../shared/DownloadExampleData";

export default function AttendanceDataExample(props) {
  const title = "Mẫu dữ liệu ds điểm danh";
  const fileName = "attendance-example.xlsx";
  const head = ["TermNote", "classid", "courseid", "name", "SectionType", "note", "StudentID", "studentname", "birthdate", "termid"];
  const body = [
    ["AB", "694598", "IT1110", "Tin học đại cương", "TN", "**CTTT-nhúng-N06C", "20193285", "Lê Duy Anh", "07/16/2001", "20192"],
    ["AB", "694598", "IT1110", "Tin học đại cương", "TN", "**CTTT-nhúng-N06C", "20193285", "Lê Duy Anh", "07/16/2001", "20192"],
    ["AB", "694598", "IT1110", "Tin học đại cương", "TN", "**CTTT-nhúng-N06C", "20193285", "Lê Duy Anh", "07/16/2001", "20192"],
    ["AB", "694598", "IT1110", "Tin học đại cương", "TN", "**CTTT-nhúng-N06C", "20193285", "Lê Duy Anh", "07/16/2001", "20192"],
  ];
  return <DownloadExampleData {...{ title, fileName, head, body }}></DownloadExampleData>;
}
