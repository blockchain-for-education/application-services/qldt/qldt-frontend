import Page from "src/shared/Page";
import AttendanceDataExample from "./AttendanceDataExample";
import DragnDropZone from "../../../shared/DragnDropZone";

import { makeStyles } from "@material-ui/core";
import { useSnackbar } from "notistack";
import { useDispatch, useSelector } from "react-redux";
import { getToken } from "src/utils/mng-token";
import { startUploadFile, uploadFileFail, uploadFileSuccess } from "./redux";
import { requirePrivateKeyHex } from "../../../utils/keyholder";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginBottom: theme.spacing(2),
      "&:last-child": {
        marginBottom: 0,
      },
    },
    paddingBottom: theme.spacing(2.5),
  },
}));

export default function UploadAttendance(props) {
  const cls = useStyles();
  const uploading = useSelector((state) => state.attendanceSlice.uploading);
  const dp = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  async function hdUploadFile(files) {
    const privateKeyHex = await requirePrivateKeyHex(enqueueSnackbar);
    dp(startUploadFile());
    const formData = new FormData();
    formData.append("excel-file", files[0]);
    formData.append("privateKeyHex", privateKeyHex);
    const response = await fetch(`${process.env.REACT_APP_SERVER_URL}/staff/upload-attendance`, {
      method: "POST",
      headers: { Authorization: getToken() },
      body: formData,
    });
    if (!response.ok) {
      const msg = await response.text();
      dp(uploadFileFail());
      enqueueSnackbar("Something went wrong: " + msg, { variant: "error", anchorOrigin: { vertical: "top", horizontal: "center" } });
    } else {
      const result = await response.json();
      dp(uploadFileSuccess(result));
      enqueueSnackbar("Result: " + JSON.stringify(result), { variant: "success", anchorOrigin: { vertical: "bottom", horizontal: "center" } });
    }
  }

  return (
    <Page title="Upload ds điểm danh">
      <div className={cls.root}>
        <AttendanceDataExample></AttendanceDataExample>
        <DragnDropZone onDropAccepted={hdUploadFile} uploading={uploading}></DragnDropZone>
      </div>
    </Page>
  );
}
