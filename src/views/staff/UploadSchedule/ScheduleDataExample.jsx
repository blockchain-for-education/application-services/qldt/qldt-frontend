import DownloadExampleData from "../../../shared/DownloadExampleData";

export default function ScheduleDataExample(props) {
  const title = "Mẫu file dữ liệu phân công giảng dạy";
  const fileName = "schedule-example.xlsx";
  const head = ["SchoolName", "Semester", "ClassID", "CourseID", "ClassName", "CreditInfo", "ClassNote", "Program", "SemesterType", "..."];
  const body = [
    ["KCNTT", "20201", "118161", "IT3180", "Nhập môn công nghệ phần mềm", "	3(2-2-0-6)", "**Tài năng-CNTT-K63C", "KSTN", "AB"],
    ["KCNTT", "20201", "118161", "IT3180", "Nhập môn công nghệ phần mềm", "	3(2-2-0-6)", "**Tài năng-CNTT-K63C", "KSTN", "AB"],
    ["KCNTT", "20201", "118161", "IT3180", "Nhập môn công nghệ phần mềm", "	3(2-2-0-6)", "**Tài năng-CNTT-K63C", "KSTN", "AB"],
    ["KCNTT", "20201", "118161", "IT3180", "Nhập môn công nghệ phần mềm", "	3(2-2-0-6)", "**Tài năng-CNTT-K63C", "KSTN", "AB"],
  ];
  return <DownloadExampleData {...{ title, fileName, head, body }}></DownloadExampleData>;
}
