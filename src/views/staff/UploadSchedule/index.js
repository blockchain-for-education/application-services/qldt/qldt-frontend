import Page from "src/shared/Page";
import ScheduleDataExample from "./ScheduleDataExample";
import DragnDropZone from "../../../shared/DragnDropZone";

import { makeStyles } from "@material-ui/core";
import { useSnackbar } from "notistack";
import { useDispatch, useSelector } from "react-redux";
import { getToken } from "src/utils/mng-token";
import { startUploadFile, uploadFileFail, uploadFileSuccess } from "./redux";
import { requirePrivateKeyHex } from "../../../utils/keyholder";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginBottom: theme.spacing(2),
      "&:last-child": {
        marginBottom: 0,
      },
    },
    paddingBottom: theme.spacing(2.5),
  },
}));

export default function UploadSchedule(props) {
  const cls = useStyles();
  const uploading = useSelector((state) => state.scheduleSlice.uploading);
  const dp = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  async function hdUploadFile(files) {
    const privateKeyHex = await requirePrivateKeyHex(enqueueSnackbar);
    dp(startUploadFile());
    const formData = new FormData();
    formData.append("excel-file", files[0]);
    formData.append("privateKeyHex", privateKeyHex);
    const response = await fetch(`${process.env.REACT_APP_SERVER_URL}/staff/upload-schedule`, {
      method: "POST",
      headers: { Authorization: getToken() },
      body: formData,
    });
    if (!response.ok) {
      const msg = await response.text();
      dp(uploadFileFail());
      enqueueSnackbar(msg, { variant: "error", anchorOrigin: { vertical: "top", horizontal: "center" } });
    } else {
      const result = await response.json();
      dp(uploadFileSuccess(result));
      enqueueSnackbar("Upload file thành công!", { variant: "success", anchorOrigin: { vertical: "bottom", horizontal: "center" } });
    }
  }

  return (
    <Page title="Upload phân công giảng dạy">
      <div className={cls.root}>
        <ScheduleDataExample></ScheduleDataExample>
        <DragnDropZone onDropAccepted={hdUploadFile} uploading={uploading}></DragnDropZone>
      </div>
    </Page>
  );
}
