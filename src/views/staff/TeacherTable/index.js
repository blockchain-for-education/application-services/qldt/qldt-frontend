import { makeStyles } from "@material-ui/core";
import { DataGrid } from "@material-ui/data-grid";
import { useSnackbar } from "notistack";
import { useEffect, useState } from "react";
import Page from "../../../shared/Page";
import axios from "axios";

const columns = [
  { field: "id", headerName: "#", width: 80, type: "string" },
  { field: "teacherId", headerName: "Mã Giảng Viên", width: 150, type: "string" },
  { field: "name", headerName: "Họ và  Tên", width: 200, type: "string" },
  { field: "email", headerName: "Email", width: 200, type: "string" },
  { field: "firstTimePassword", headerName: "Mật khẩu", width: 150 },
  { field: "department", headerName: "Khoa/Viện", type: "string" },
  { field: "phone", headerName: "SĐT", width: 150, type: "string" },
  { field: "publicKey", headerName: "Khóa công khai", width: 300 },
  { field: "txid", headerName: "Txid", width: 300 },
];

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "white",
  },
}));

export default function TeacherTable(props) {
  const cls = useStyles();
  const [state, setState] = useState({
    fetching: true,
    teachers: [],
  });
  const { enqueueSnackbar } = useSnackbar();
  useEffect(() => {
    fetchTeachers();
  }, []);

  async function fetchTeachers() {
    try {
      const response = await axios.get("/staff/teachers");
      const teachers = response.data.map((teacher, index) => ({ ...teacher, id: index + 1 }));
      setState({ fetching: false, teachers: response.data.length > 0 ? teachers : [] });
    } catch (error) {
      enqueueSnackbar("Fail to load teacher" + error.response.data, { variant: "error", anchorOrigin: { vertical: "top", horizontal: "center" } });
    }
  }
  return (
    <Page title="Danh sách giảng viên">
      <DataGrid className={cls.root} loading={state.fetching} rows={state.teachers} columns={columns} rowHeight={48} pageSize={50} autoHeight rowsPerPageOptions={[5, 10, 25, 50, 100]}></DataGrid>
    </Page>
  );
}
