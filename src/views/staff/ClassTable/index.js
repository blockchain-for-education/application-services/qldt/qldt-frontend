import { Box, Button, Divider, Link, makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@material-ui/core";
import { useState } from "react";
import Page from "../../../shared/Page";
import { getToken } from "../../../utils/mng-token";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      marginBottom: theme.spacing(2),
    },
  },
}));

export default function ClassAndStudentInfo(props) {
  const cls = useStyles();
  const [classId, setClassId] = useState("");
  const [claxx, setClaxx] = useState(null);

  async function hdFetchClass(e) {
    try {
      const response = await fetch(`${process.env.REACT_APP_SERVER_URL}/staff/classes/${classId}`, {
        headers: { Authorization: getToken() },
      });
      const result = await response.json();
      if (!response.ok) {
        alert(JSON.stringify(await response.json()));
      } else {
        setClaxx(result);
      }
    } catch (err) {
      console.log(err);
      alert(err);
    }
  }

  return (
    <Page title="Thông tin lớp học" h100={false}>
      <Box className={cls.root}>
        <Paper>
          <Box px={2} py={2} display="flex" alignItems="flex-end">
            <TextField label="Mã lớp" InputLabelProps={{ shrink: true }} autoFocus placeholder="118161" value={classId} onChange={(e) => setClassId(e.target.value)}></TextField>
            <Box px={2}>
              <Button variant="contained" color="primary" onClick={hdFetchClass}>
                Go
              </Button>
            </Box>
          </Box>
        </Paper>
        {claxx && (
          <>
            {/* Subject Info */}
            <Paper>
              <Box px={2} pt={2} pb={1}>
                <Typography variant="h5">{`Thông tin môn học`}</Typography>
              </Box>
              <Divider></Divider>
              <TableContainer>
                <Table size="small">
                  <TableBody>
                    <TableRow>
                      <TableCell>Mã môn học</TableCell>
                      <TableCell>{claxx.subject.subjectId}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Tên môn học</TableCell>
                      <TableCell>{claxx.subject.name}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Tín chỉ</TableCell>
                      <TableCell>{claxx.subject.credit}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Ghi chú</TableCell>
                      <TableCell>{claxx.subject.note}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Paper>
            {/* Teacher Info */}
            <Paper>
              <Box px={2} pt={2} pb={1}>
                <Typography variant="h5">{`Thông tin giảng viên`}</Typography>
              </Box>
              <Divider></Divider>
              <TableContainer>
                <Table size="small">
                  <TableBody>
                    <TableRow>
                      <TableCell>Giảng Viên</TableCell>
                      <TableCell>{claxx.teacher.name}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Email</TableCell>
                      <TableCell>{claxx.teacher.email}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>SDT</TableCell>
                      <TableCell>{claxx.teacher.phone}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Khoa/Viện</TableCell>
                      <TableCell>{claxx.teacher.department}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Paper>
          </>
        )}
        {claxx && claxx.students && (
          <Paper>
            {/* Student List  */}
            <Box px={2} pt={2} pb={1}>
              <Typography variant="h5">{`Danh sách sinh viên`}</Typography>
            </Box>
            <Divider></Divider>
            <TableContainer>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>#</TableCell>
                    <TableCell>Mssv</TableCell>
                    <TableCell>Họ và tên</TableCell>
                    {/* <TableCell>Ngày sinh</TableCell> */}
                    <TableCell>Lớp</TableCell>
                    <TableCell style={{ width: "100px" }}>Điểm GK</TableCell>
                    <TableCell style={{ width: "100px" }}>Điểm CK</TableCell>
                    <TableCell style={{ width: "150px" }}>Txid</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {claxx.students.map((student, index) => (
                    <TableRow key={index}>
                      <TableCell>{index + 1}</TableCell>
                      <TableCell>{student.studentId}</TableCell>
                      <TableCell>{student.name}</TableCell>
                      <TableCell>{student.class}</TableCell>
                      <TableCell>{student.halfSemesterPoint || "Chưa có"}</TableCell>
                      <TableCell>{student.finalSemesterPoint || "Chưa có"}</TableCell>
                      <TableCell>{getLinkFromTxid(student.txid)}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        )}
        {claxx && !claxx?.students && (
          <Paper>
            <Box p={2}>
              <Typography>Chưa có thông tin danh sách sinh viên!</Typography>
            </Box>
          </Paper>
        )}
      </Box>
    </Page>
  );
}

function getLinkFromTxid(txid) {
  if (txid) {
    return (
      <Link target="_blank" href={`${process.env.REACT_APP_EXPLORER_URL}/#/transactions/${txid}`}>
        {txid.slice(0, 20)}...
      </Link>
    );
  } else {
    return null;
  }
}
