import { combineReducers, configureStore } from "@reduxjs/toolkit";
import profileReducer from "src/views/staff/MakeRequest/redux";

import certificateReducer from "src/views/staff/UploadCertificate/redux";
import scheduleReducer from "src/views/staff/UploadSchedule/redux";
import attendanceReducer from "src/views/staff/UploadAttendance/redux";

import teacherProfileReducer from "src/views/teacher/Profile/redux";

export const resetStore = () => {
  return {
    type: "RESET_STORE",
  };
};

const rootReducer = (state, action) => {
  if (action.type === "RESET_STORE") {
    state = undefined;
  }
  return appReducer(state, action);
};

const appReducer = combineReducers({
  profileSlice: profileReducer,
  certificateSlice: certificateReducer,
  scheduleSlice: scheduleReducer,
  attendanceSlice: attendanceReducer,
  teacherProfileSlice: teacherProfileReducer,
});

export default configureStore({
  reducer: rootReducer,
});
