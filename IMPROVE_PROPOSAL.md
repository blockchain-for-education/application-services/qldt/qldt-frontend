# FIX

1. Upload 179 Teacher, docker-console log about 2minutes -> avoid log if not necersary

# TODO

1. Implement revoke certificate
2. Implement statistic, report
3. Notification when voting state change?
4. Add EduProgram? -> Pre-check when upload certificate
5. Add voting history?

---

# IMPROVE

1. Request Join

- Avatar: allow user crop, choose position...
- Send avatar image too!
- Voting State: show real school avatar

2. Create Bureau, Teacher, Student Account

- Allow upload multi excel file?
- Pagination History
- Change notistack text

3. Upload Subject, Class, Certificate

- Consider use other table template
- Change notistack text
